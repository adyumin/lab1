﻿#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
#include "..\CircleLib\Circle.h"
#include <functional>

namespace Lab1VSUnitTest
{
	TEST_CLASS(UnitTestPoint)
	{
	public:

		TEST_METHOD(CheckConstructorDefaultInitialization)
		{
			// TODO: Your test code here
			Figures::Point t;
			Assert::AreEqual(0.0, t.x, L"x coordinate not equal 0.0 after Point default constructor", LINE_INFO());
			Assert::AreEqual(0.0, t.y, L"y coordinate not equal 0.0 after Point default constructor", LINE_INFO());

		}
		TEST_METHOD(CheckConstructorInitialization)
		{
			// фреймворк позволяет не писать поясняющую строку, но это является плохим стилем для сложных тестов (с множественными проверками),
			// т.к. в результате будет сложно понять, какая из проверок привела к ошибке
			// опять же, можно не добавлять информацию о номере строки (для чего мы используем макрос LINE_INFO())
			// см. примеры ниже, вы можете провалить некоторые тесты и посмотреть отличия в информации доступной вам в Test Explorer

			Figures::Point t1(7.0, 1.0);
			Assert::AreEqual(7.0, t1.x, L"x coordinate not equal 7.0 after Point constructor with params 7.0, 1.0", LINE_INFO());
			Assert::AreEqual(1.0, t1.y, L"y coordinate not equal 1.0 after Point constructor with params 7.0, 1.0", LINE_INFO());
			Figures::Point t2(-5.0, 4.0);
			Assert::AreEqual(-5.0, t2.x, L"x coordinate not equal -5.0 after Point constructor with params -5.0, 4.0");
			Assert::AreEqual(4.0, t2.y, L"y coordinate not equal 4.0 after Point constructor with params -5.0, 4.0");
			Figures::Point t3(-5.0, -2.0);
			Assert::AreEqual(-5.0, t3.x, nullptr, LINE_INFO());
			Assert::AreEqual(-2.0, t3.y, nullptr, LINE_INFO());
			Figures::Point t4(-5.0, -1.255);
			Assert::AreEqual(-5.0, t4.x);
			Assert::AreEqual(-1.255, t4.y);
		}
		TEST_METHOD(CheckCopyConstructor)
		{
			Figures::Point t, tt(t);
			Figures::Point t1(7.0, 1.0), tt1(t1);
			Assert::AreEqual(7.0, t1.x);
			Assert::AreEqual(1.0, t1.y);
			Figures::Point t2(-5.0, 4.0), tt2(t2);
			Assert::AreEqual(-5.0, t2.x);
			Assert::AreEqual(4.0, t2.y);
			Figures::Point t3(-5.0, -2.0), tt3(t3);
			Assert::AreEqual(-5.0, t3.x);
			Assert::AreEqual(-2.0, t3.y);
			Figures::Point t4(-5.0, -1.255), tt4(t4);
			Assert::AreEqual(-5.0, t4.x);
			Assert::AreEqual(-1.255, t4.y);
		}

	};

	TEST_CLASS(UnitTestCircleConstuction)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			using Figures::Point;
			using Figures::Circle;
			Point t1(7.0, 1.0);
			Point t2(-5.0, 4.0);
			Point t3(-5.0, -2.0);
			Point t4(-5.0, -1.255);
			{
				// В данном фреймворке мы не можем в явном виде проконтролировать, что метод не должен генерировать исключение (если мы его не ожидаем увидеть),
				// но если какой-то метод генерирует исключение и оно не обрабатывется в TEST_METHOD, то данный тест провалится
				// Вы можете снять комментарии со следующей сторки и посмотреть, что получите в Test Explorer
				// Circle (2,2,-2);

				Circle tt1(t1);
				Assert::AreEqual(7.0, tt1.getP().x);
				Assert::AreEqual(1.0, tt1.getP().y);
				Assert::AreEqual(1.0, tt1.getR());
				Circle tt2(t2);
				Assert::AreEqual(-5.0, tt2.getP().x);
				Assert::AreEqual(4.0, tt2.getP().y);
				Assert::AreEqual(1.0, tt2.getR());
				Circle tt3(t3);
				Assert::AreEqual(-5.0, tt3.getP().x);
				Assert::AreEqual(-2.0, tt3.getP().y);
				Assert::AreEqual(1.0, tt3.getR());
				Circle tt4(t4);
				Assert::AreEqual(-5.0, tt4.getP().x);
				Assert::AreEqual(-1.255, tt4.getP().y);
				Assert::AreEqual(1.0, tt4.getR());
			}
			for (int i = 0; i < 1000; ++i)
			{
				double x = (rand() % 1000) * (rand() % 1000) / double(rand() % 10000);
				double y = (rand() % 1000) * (rand() % 1000) / double(rand() % 10000);
				Point t(x, y);
				Circle tt(t);
				Assert::AreEqual(x, tt.getP().x);
				Assert::AreEqual(y, tt.getP().y);
				Assert::AreEqual(1.0, tt.getR());
			}

			{
				Circle tt1(t1, 5.5);
				Assert::AreEqual(7.0, tt1.getP().x);
				Assert::AreEqual(1.0, tt1.getP().y);
				Assert::AreEqual(5.5, tt1.getR());
				Circle tt2(t2, 0.0);
				Assert::AreEqual(-5.0, tt2.getP().x);
				Assert::AreEqual(4.0, tt2.getP().y);
				Assert::AreEqual(0.0, tt2.getR());
				Circle tt3(t3, 100.17);
				Assert::AreEqual(-5.0, tt3.getP().x);
				Assert::AreEqual(-2.0, tt3.getP().y);
				Assert::AreEqual(100.17, tt3.getR());
				Circle tt4(t4, 2.456);
				Assert::AreEqual(-5.0, tt4.getP().x);
				Assert::AreEqual(-1.255, tt4.getP().y);
				Assert::AreEqual(2.456, tt4.getR());
			}
			{
				/* Опять же в данном фреймворке мы можем проверить только тип возвращаемого исключения, но не реальное значение объекта */
				std::function<void(void)> f1 = [t1] {Circle(t1, -10.3); };
				std::function<void(void)> f2 = [t1] {Circle(t1, -0.0001); };
				std::function<void(void)> f3 = [t1] {Circle(t1, -1.242e20); };
				Assert::ExpectException <const char *>(f1, L"Radius -10.3 is not generate exception", LINE_INFO());
				Assert::ExpectException <const char *>(f2, L"Radius -0.0001 is not generate exception ", LINE_INFO());
				Assert::ExpectException <const char *>(f3, L"Radius -1.242e20 is is not generate exception", LINE_INFO());
			}

		}
		// Добавьте ваши тесты для проверки оставшихся конструторов и get- и set-методов
	};

	TEST_CLASS(CircleCalculationgMethodsTesting)
	{
	public:
		TEST_METHOD(AreaTesting)
		{
			using Figures::Circle;
			const double PI = acos(-1.0); //3.1415926535897931;
			//EXPECT_DOUBLE_EQ(PI, acos(-1.0));
			Circle t0(-4635.345, 23.3, 0.0);
			Assert::AreEqual(t0.area(), 0.0);
			Circle t1(8.43, 1.3e-10, 1.0);
			Assert::AreEqual(PI, t1.area(), 0.00001);
			Circle t2(1.0, 17.12, 2.5);
			Assert::AreEqual(PI*2.5*2.5, t2.area(), 0.0001); // <--- снижаем точность, см. комментарии в предыдущем проекте
			Circle t3(1.0, 17.12, 500.0);
			Assert::AreEqual(PI*500.0*500.0, t3.area(), 1.0); 

		}

		/*... Добавьте ваши тесты для проверки других вычислительных методов  ...*/
	};


	TEST_CLASS(CircleStringCreation)
	{
	public:
		TEST_METHOD(FormatTest)
		{
		
			Figures::Circle t1(1.0, -2.0, 12);
			const size_t len1 = 80, len2 = 10;
			char st1[len1] = "";
			_set_errno(0);
			Assert::AreEqual(41, t1.frm(st1, len1));
			Assert::AreEqual(0, errno);
			Assert::AreEqual("(x - 1.00) ^ 2 + (y - -2.00) ^ 2 = 144.00", st1);
			_set_errno(0);

			// Я здесь не привожу тесты для проверки корректности поведения Circle::frm(...) в случае ошибок, но вы можете дописать их сами:)
			// Мы не можем проверить "смерть" приложения в этом тестевом фреймворке,
			// но мы можем проверить другие "эффекты", если предоставим собственный обработчик на некорректные параметры 
			// (для справки смотрите проект для Google С++ Testing Framework).

			// Добавьте свои тесты для тестирования методы формирвания строки с уравнением окружности
		}
	};
}