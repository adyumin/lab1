﻿// Lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "..\CircleLib\circle.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	// Пример простейшей прикладной программы, 
	// в ваших прикладных программах должен быть реализован диалог с пользователем,
	// который позволяет проверить весь функционал разработанного класса
	// (в произвольном порядке и с произвольными параметрами фигур)
	using std::wcout;
	using std::endl;
	Figures::Circle test;
	char equation[80];
	test.frm(equation, 80);
	wcout << L"Circle's equation: " << equation << endl;
	wcout << L"The circle's perimeter is " << test.perimeter() << endl;
	wcout << L"The circle's area is " << test.area() << endl;

	return 0;
}

