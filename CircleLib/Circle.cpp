﻿#include "stdafx.h"
#include "Circle.h"
#include <cmath>
#include <cstdio>

using namespace Figures;

Circle::Circle():p(0, 0), r(1)
{
}

Circle::~Circle()
{
}

Circle::Circle(const Point &p0, double rad) :p(p0), r(rad)
{
	setR(rad);
}
Circle::Circle(double x0, double y0, double rad) : p(x0, y0)
{
	setR(rad);
}
Circle& Circle::setP(const Point &p0)
{
	p = p0;
	return *this;
}
Circle& Circle::setR(double r0)
{
	// Генерация исключений типа const char * является плохим стилем программирования, лучше использовать для этих целей специальную иерархию классов - исключений
	// Но т.к. данный пример написан по мотивам вашего первого семинара по ООП, мы не перегружаем его дополнительной иерархией классов
	
	if (r0 < 0)
		throw "illegal radius value";
	r = r0;
	return *this;
}

Point Circle::getP() const
{
	return p; 
}
double Circle::getR() const
{
	return r; 
}

double Circle::area() const
{
	// мы используем заведомо не очень точное значение pi,
	// будем считать, что исходя из предметной области, такая точность нам достаточна  
	return 3.14159*r*r; 
} 
double Circle::perimeter() const
{
	return 2 * 3.14159*r;
} 
double Circle::f(double x) const 
{
	double dx = x - p.x;
	dx = r * r - dx * dx;
	if (dx < 0)
		throw "illegal argument x";
	return p.y + sqrt(dx);
}
double Circle::distance() const
{
	return sqrt(p.x * p.x + p.y * p.y);
} 
int Circle::frm(char *s, size_t len) const 
{
	//используем безопасную версию функции sprintf
	return sprintf_s(s, len, "(x - %.2f) ^ 2 + (y - %.2f) ^ 2 = %.2f", p.x, p.y, r*r);
}
