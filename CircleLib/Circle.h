﻿#pragma once
// Пример простейшего класса, рассматривавшийся на 1ой лабораторной работе.
// Обратите внинмание, что интерфейс класса немного отличается, от того что был на лабораторной.

namespace Figures
{
	struct Point{
		double x, y;
		Point(double x0 = 0, double y0 = 0) :x(x0), y(y0){}
	};

	class Circle{
	private:
		Point p;
		double r;
	public:
		Circle();
		virtual ~Circle();
		Circle(const Point &p0, double rad = 1);
		Circle(double x0, double y0, double rad = 1);
		Circle& setP(const Point &p0);
		Circle& setR(double r0);
		Point getP() const;
		double getR() const;
		double area() const;
		double perimeter() const;
		double f(double x) const;
		double distance() const;
		int frm(char *s, size_t len) const;
	};
};